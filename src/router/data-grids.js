import Layout2 from '@/layout/Layout2'

export default [{
  path: '/calls',
  component: Layout2,
  children: [{
    path: '',
    component: () => import('@/components/pages/Calls')
  }]
}]
