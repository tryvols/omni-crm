import LayoutBlank from '@/layout/LayoutBlank'

export default [{
  path: '/authentication',
  component: LayoutBlank,
  children: [{
    path: 'login',
    component: () => import('@/components/pages/authentication/Login')
  }]
}]
